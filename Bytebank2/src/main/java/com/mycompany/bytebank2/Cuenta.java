/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bytebank2;

/**
 *
 * @author Usuario
 */
class Cuenta {

    double salario;
    int numero;
    int agencia;
    String titular;

    void depositar(double valor) {
        this.salario = this.salario + valor;
    }

    public boolean retirar(double valor) {
        if (this.salario >= valor) {
            this.salario -= valor;
            return true; 
        }

        else{
            return false;
            }
    }

    public boolean transferir (double valor, Cuenta cuenta){
        if (this.salario >= valor){
            this.salario = this.salario - valor;
            cuenta.depositar(valor);
            return true;
        }

        return false;
    }
    
    
}
