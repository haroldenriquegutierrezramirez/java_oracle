/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bytebank2;

/**
 *
 * @author Usuario
 */
public class TestReferencia {
    public static void main(String[] args) {
         
    Cuenta primeraCuenta = new Cuenta();
    primeraCuenta.salario = 200;
    
    Cuenta segundaCuenta = primeraCuenta; // apuntando a la misma direccion en memoria
    segundaCuenta.salario = 100;
    
        System.out.println("Primera Cuenta: " + primeraCuenta.salario);
        System.out.println(" segundaCuenta: " + segundaCuenta.salario);
        
        segundaCuenta.salario += 400;
        System.out.println(" Primera Cuenta: " + primeraCuenta.salario);
        
        System.out.println(primeraCuenta);
        System.out.println(segundaCuenta);
        
        
        
    }
}
