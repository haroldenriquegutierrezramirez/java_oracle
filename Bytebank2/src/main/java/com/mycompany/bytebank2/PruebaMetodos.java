/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bytebank2;

/**
 *
 * @author Usuario
 */
public class PruebaMetodos {
    
    public static void main(String[] args) {
        
        Cuenta miCuenta = new Cuenta();
        miCuenta.salario = 400;
        miCuenta.depositar(200);
        
        System.out.println(miCuenta.salario);
        miCuenta.retirar(100);
        System.out.println(miCuenta.salario);

        Cuenta cuentadeJimena = new Cuenta();
        cuentadeJimena.depositar(1000);
        boolean puedeTransferir = cuentadeJimena.transferir(400, miCuenta);
        if (puedeTransferir){
            System.out.println("Transferencia Exitosa");
        }
        else {
            System.out.println("Transferencia Fallida");
        }


    }
}
